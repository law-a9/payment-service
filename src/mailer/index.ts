import * as SibApiV3Sdk from "sib-api-v3-sdk";
import * as config  from "../config";

SibApiV3Sdk.ApiClient.instance.authentications["api-key"].apiKey = config.SIB_API_KEY;
const mailer = new SibApiV3Sdk.TransactionalEmailsApi();

interface attachment {
  content: string,
  name: string
}

export const sendEmail = async (recipient: string, subject: string, message: string, attachment: attachment[] | null = null) => {
  const opts = {
    "subject": subject,
    "sender": { "email": "no-reply@antriklinik.pacil.tech", "name": "AntriKlinik" },
    "to": [{ "email": recipient }],
    "htmlContent": message
  };

  if (attachment) {
    opts["attachment"] = attachment;
  }

  await mailer.sendTransacEmail(opts);


};
