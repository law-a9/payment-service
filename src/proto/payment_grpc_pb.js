// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var payment_pb = require('./payment_pb.js');

function serialize_Empty(arg) {
  if (!(arg instanceof payment_pb.Empty)) {
    throw new Error('Expected argument of type Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_Empty(buffer_arg) {
  return payment_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_InvoiceOrder(arg) {
  if (!(arg instanceof payment_pb.InvoiceOrder)) {
    throw new Error('Expected argument of type InvoiceOrder');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_InvoiceOrder(buffer_arg) {
  return payment_pb.InvoiceOrder.deserializeBinary(new Uint8Array(buffer_arg));
}


var SendInvoiceService = exports.SendInvoiceService = {
  sendInvoice: {
    path: '/SendInvoice/SendInvoice',
    requestStream: false,
    responseStream: false,
    requestType: payment_pb.InvoiceOrder,
    responseType: payment_pb.Empty,
    requestSerialize: serialize_InvoiceOrder,
    requestDeserialize: deserialize_InvoiceOrder,
    responseSerialize: serialize_Empty,
    responseDeserialize: deserialize_Empty,
  },
};

exports.SendInvoiceClient = grpc.makeGenericClientConstructor(SendInvoiceService);
