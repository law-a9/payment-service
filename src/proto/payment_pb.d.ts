// package: 
// file: payment.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class ObatOrder extends jspb.Message { 
    getNama(): string;
    setNama(value: string): ObatOrder;
    getDosis(): string;
    setDosis(value: string): ObatOrder;
    getJumlah(): number;
    setJumlah(value: number): ObatOrder;
    getHargasatuan(): number;
    setHargasatuan(value: number): ObatOrder;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ObatOrder.AsObject;
    static toObject(includeInstance: boolean, msg: ObatOrder): ObatOrder.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ObatOrder, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ObatOrder;
    static deserializeBinaryFromReader(message: ObatOrder, reader: jspb.BinaryReader): ObatOrder;
}

export namespace ObatOrder {
    export type AsObject = {
        nama: string,
        dosis: string,
        jumlah: number,
        hargasatuan: number,
    }
}

export class InvoiceOrder extends jspb.Message { 
    getUid(): string;
    setUid(value: string): InvoiceOrder;
    clearOrderList(): void;
    getOrderList(): Array<ObatOrder>;
    setOrderList(value: Array<ObatOrder>): InvoiceOrder;
    addOrder(value?: ObatOrder, index?: number): ObatOrder;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): InvoiceOrder.AsObject;
    static toObject(includeInstance: boolean, msg: InvoiceOrder): InvoiceOrder.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: InvoiceOrder, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): InvoiceOrder;
    static deserializeBinaryFromReader(message: InvoiceOrder, reader: jspb.BinaryReader): InvoiceOrder;
}

export namespace InvoiceOrder {
    export type AsObject = {
        uid: string,
        orderList: Array<ObatOrder.AsObject>,
    }
}

export class Empty extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Empty.AsObject;
    static toObject(includeInstance: boolean, msg: Empty): Empty.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Empty, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Empty;
    static deserializeBinaryFromReader(message: Empty, reader: jspb.BinaryReader): Empty;
}

export namespace Empty {
    export type AsObject = {
    }
}
