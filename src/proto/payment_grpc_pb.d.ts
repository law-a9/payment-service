// package:
// file: payment.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as payment_pb from "./payment_pb";

export interface ISendInvoiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
  sendInvoice: ISendInvoiceService_ISendInvoice;
}

interface ISendInvoiceService_ISendInvoice extends grpc.MethodDefinition<payment_pb.InvoiceOrder, payment_pb.Empty> {
  path: "/SendInvoice/SendInvoice";
  requestStream: false;
  responseStream: false;
  requestSerialize: grpc.serialize<payment_pb.InvoiceOrder>;
  requestDeserialize: grpc.deserialize<payment_pb.InvoiceOrder>;
  responseSerialize: grpc.serialize<payment_pb.Empty>;
  responseDeserialize: grpc.deserialize<payment_pb.Empty>;
}

export const SendInvoiceService: ISendInvoiceService;

export interface ISendInvoiceServer {
  sendInvoice: grpc.handleUnaryCall<payment_pb.InvoiceOrder, payment_pb.Empty>;
}

export interface ISendInvoiceClient {
  sendInvoice(request: payment_pb.InvoiceOrder, callback: (error: grpc.ServiceError | null, response: payment_pb.Empty) => void): grpc.ClientUnaryCall;

  sendInvoice(request: payment_pb.InvoiceOrder, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: payment_pb.Empty) => void): grpc.ClientUnaryCall;

  sendInvoice(request: payment_pb.InvoiceOrder, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: payment_pb.Empty) => void): grpc.ClientUnaryCall;
}

export class SendInvoiceClient extends grpc.Client implements ISendInvoiceClient {
  constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);

  public sendInvoice(request: payment_pb.InvoiceOrder, callback: (error: grpc.ServiceError | null, response: payment_pb.Empty) => void): grpc.ClientUnaryCall;
  public sendInvoice(request: payment_pb.InvoiceOrder, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: payment_pb.Empty) => void): grpc.ClientUnaryCall;
  public sendInvoice(request: payment_pb.InvoiceOrder, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: payment_pb.Empty) => void): grpc.ClientUnaryCall;
}
