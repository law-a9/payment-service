import { ISendInvoiceServer, SendInvoiceService } from "../proto/payment_grpc_pb";
import { Empty, InvoiceOrder } from "../proto/payment_pb";
import * as grpc from "grpc";
import { logger } from "@utils/logger";
import { OrdersEntity } from "@entities/orders.entity";
import { ObatOrdersEntity } from "@entities/obatOrder.entity";
import { Worker } from "@/workerConn";

class SendInvoiceServer implements ISendInvoiceServer {
  sendInvoice: grpc.handleUnaryCall<InvoiceOrder, Empty> = async (call, callback) => {
    logger.info(`[INFO] (Payment) GRPC Connection: ${call.getPeer()} - [200 OK]`);
    console.log(call.metadata.getMap());

    let order = new OrdersEntity();
    order.uid = call.request.getUid();
    order.status = "Waiting Payment";
    order.total = 0;
    order = await order.save();
    await order.reload();

    let total = 0;

    for (const obatOrder of call.request.getOrderList()) {
      const obat = new ObatOrdersEntity();
      obat.order = order;
      obat.namaObat = obatOrder.getNama();
      obat.dosis = obatOrder.getDosis();
      obat.jumlah = obatOrder.getJumlah();
      obat.biaya = obatOrder.getJumlah() * obatOrder.getHargasatuan();
      order.obat.push(obat);
      await obat.save();

      total += obat.biaya;
    }

    order.total = total;
    order = await order.save();

    await order.reload()

    logger.info(JSON.stringify(order));
    await Worker.sendWork("genInvoice", order);

    callback(null, new Empty());
  };
}

export class GrpcServer {
  private server;

  constructor() {
    this.server = new grpc.Server();
    this.server.addService(SendInvoiceService, new SendInvoiceServer());
  }

  public listen = (port: string) => {
    this.server.bindAsync(port, grpc.ServerCredentials.createInsecure(), () => {
      this.server.start();
      console.log(`🚀 GRPC listening on the port ${port}`);
    });
  };
}
