import { Stream } from "stream";

const { Base64Encode } = require('base64-stream');
const PDFDocument = require("pdfkit-table");

import { sendEmail } from "../mailer";

import { ObatOrdersEntity } from "@entities/obatOrder.entity";
import { OrdersEntity } from "@entities/orders.entity";
import { createWorker } from "../queueWorker/index";

const formatter = new Intl.NumberFormat('id-ID', {
  style: 'currency',
  currency: 'IDR',
});

const createInvoicePdf = (user: string, total: number, status: string, orders: ObatOrdersEntity[]): Promise<string> => {
  return new Promise(resolve => {
    const doc = new PDFDocument({ margin: 30, size: 'A4' });
    let pdfString = '';
    const stream = doc.pipe(new Base64Encode()) as Stream;

    stream.on('data', chunk => {
      pdfString += chunk;
    })

    stream.on('end', () => {
      resolve(pdfString)
    })

    doc.fontSize(25);

    doc.text("Invoice");
    doc.moveDown();

    doc.fontSize(12);
    doc.text(user);
    doc.text("Status: " + status);

    doc.moveDown();

    const rows = [];
    for (const order of orders) {
      rows.push([order.namaObat, order.dosis, order.jumlah, order.biaya])
    }
    const tableArray = {
      headers: ["Nama Obat", "Dosis", "Jumlah", "Total Biaya"],
      rows: rows,
    };
    doc.table( tableArray, { width: 300, }); // A4 595.28 x 841.89 (portrait) (about width sizes)
    doc.text("Total: " + formatter.format(total));

    doc.end();
  })
}

const handleWorker = async (order: OrdersEntity) => {
  const attachment = await createInvoicePdf(order.uid, order.total, order.status, order.obat);
  await sendEmail(order.uid, "Invoice from AntriKlinik", `
  Hi,\n\n

  Please see attached invoice for your order from AntriKlinik. Your order will be processed after the invoice is paid.\n\n

  Thank you,\n
  AntriKlinik
  `, [{
    content: attachment,
    name: "Invoice.pdf"
  }])
}

createWorker('genInvoice', handleWorker)
