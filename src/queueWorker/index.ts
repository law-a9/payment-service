import { AMQP_ADDRESS } from "../config";
import * as amqp from "amqplib/callback_api";

export const createWorker = (workName: string, handler: (args: any) => void) => {
  amqp.connect(AMQP_ADDRESS, function(error0, connection) {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function(error1, channel) {
      if (error1) {
        throw error1;
      }

      channel.assertQueue(workName, {
        durable: false
      });

      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", workName);

      channel.consume(workName, function(msg) {
        console.log(" [x] Received %s", msg.content.toString());
        const args = JSON.parse(msg.content.toString());
        handler(args);
      }, {
        noAck: true
      });
    });
  });

}
