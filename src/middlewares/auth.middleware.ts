import { NextFunction, Request, Response } from 'express';
import { HttpException } from '@exceptions/HttpException';
import { logger } from '@utils/logger';
import axios from "axios";

const authMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  try {
    if (!req.headers.authorization) {
      res.sendStatus(401);
      return;
    }

    const token = req.headers.authorization.split(' ')[1];

    const config = {
      method: 'get',
      url: 'https://law-auth-service.herokuapp.com/verify',
      headers: {
        'Token': token
      }
    };

    const response = await axios(config);
    console.log(response.data)
    if ("email" in response.data) {
      console.log("True");
      next()
    } else {
      console.log("Fail");
      res.sendStatus(401);
    }

  } catch (error) {
    next(error);
  }
};

export default authMiddleware;
