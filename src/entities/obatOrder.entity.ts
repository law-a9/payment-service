import { IsNotEmpty } from "class-validator";
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { ObatOrder } from "@interfaces/orders.interface";
import { OrdersEntity } from "@entities/orders.entity";

@Entity()
export class ObatOrdersEntity extends BaseEntity implements ObatOrder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  namaObat: string;

  @Column()
  @IsNotEmpty()
  dosis: string;

  @Column()
  @IsNotEmpty()
  jumlah: number;

  @Column()
  @IsNotEmpty()
  biaya: number;

  @ManyToOne(() => OrdersEntity, order => order.obat)
  order: OrdersEntity;


}
