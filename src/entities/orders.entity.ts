import { IsNotEmpty } from 'class-validator';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  AfterLoad,
  AfterInsert,
  AfterUpdate
} from "typeorm";
import { Order } from "@interfaces/orders.interface";
import { ObatOrdersEntity } from "@entities/obatOrder.entity";

@Entity()
export class OrdersEntity extends BaseEntity implements Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  total: number;

  @Column()
  @IsNotEmpty()
  uid: string;

  @Column()
  @IsNotEmpty()
  status: string;

  @OneToMany(() => ObatOrdersEntity, obatOrder => obatOrder.order, {
    eager: true,
    cascade: true
  })
  obat: ObatOrdersEntity[];

  // eslint-disable-next-line @typescript-eslint/require-await
  @AfterLoad()
  @AfterInsert()
  @AfterUpdate()
  async nullChecks() {
    if (!this.obat) {
      this.obat = []
    }
  }

}
