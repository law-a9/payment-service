import { config } from "dotenv";
import * as Path from "path";

config({ path: Path.join(__dirname, '..', '..', `.env.${process.env.NODE_ENV || "development"}.local`) });

export const CREDENTIALS = process.env.CREDENTIALS === "true";
export const {
  NODE_ENV,
  PORT,
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PASSWORD,
  DB_DATABASE,
  SECRET_KEY,
  LOG_FORMAT,
  LOG_DIR,
  ORIGIN,
  GRPC_PORT,
  AMQP_ADDRESS,
  SIB_API_KEY,
  LOG_ADDR,
  LOG_PORT,
} = process.env;
