import { AMQP_ADDRESS } from "@config";
import * as amqp from "amqplib/callback_api";

export class Worker {
  private static connection;
  private static channel;

  public static sendWork = async (workName: string, args: any) => {
    if (!Worker.channel) {
      Worker.connection = await new Promise((resolve, reject) => {
        amqp.connect(AMQP_ADDRESS, function(err, conn) {
          if (err) reject(err);
          resolve(conn);
        });
      });

      Worker.channel = await new Promise((resolve, reject) => {
        Worker.connection.createChannel(function(err, channel) {
          if (err) reject(err);
          resolve(channel);
        });
      });
    }

    Worker.channel.assertQueue(workName, {
      durable: false
    });

    Worker.channel.sendToQueue(workName, Buffer.from(JSON.stringify(args)));
  };

  public static cleanUp = () => {
    Worker.connection.close();
    Worker.connection = null;
    Worker.channel = null;
  };
}
