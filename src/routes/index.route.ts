import { Router } from 'express';
import IndexController from '@controllers/index.controller';
import { Routes } from '@interfaces/routes.interface';
import authMiddleware from "@middlewares/auth.middleware";

class IndexRoute implements Routes {
  public path = '/';
  public router = Router();
  public indexController = new IndexController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.indexController.index);
    this.router.get(`/order/`, authMiddleware, this.indexController.listOrder);
    this.router.get(`/order/:orderId/`, authMiddleware, this.indexController.getOrder);
    this.router.post(`/order/:orderId/pay/`, authMiddleware, this.indexController.payOrder);
  }
}

export default IndexRoute;
