export interface Order {
  total: number,
  uid: string,
  id: number,
  status: string,
  obat: ObatOrder[],
}

export interface ObatOrder {
  namaObat: string,
  dosis: string,
  jumlah: number,
  biaya: number
}
