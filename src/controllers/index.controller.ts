import { NextFunction, Request, Response } from 'express';
import { jwtDecodeUser } from "@utils/util";
import { OrdersEntity } from "@entities/orders.entity";

class IndexController {
  public index = (req: Request, res: Response, next: NextFunction): void => {
    try {
      res.sendStatus(200);
    } catch (error) {
      next(error);
    }
  };

  public listOrder = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const token = req.headers.authorization.split(' ')[1];
      const user = jwtDecodeUser(token);

      const data = await OrdersEntity.find({
        uid: user.email
      });

      res.send(data)
    } catch (e) {
      res.sendStatus(500);
    }
  };

  public getOrder = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const token = req.headers.authorization.split(' ')[1];
      const user = jwtDecodeUser(token);

      const data = await OrdersEntity.findOne({
        id: parseInt(req.params['orderId']),
      });


      if (!data) {
        res.status(404).send({
          code: 404,
          message: "Not Found"
        })
      } else {
        if (data.uid != user.email) {
          res.status(403).send({
            code: 403,
            message: "Not Authorized"
          })
        } else {
          res.send(data)
        }
      }
    } catch (e) {
      res.sendStatus(500);
    }
  };

  public payOrder = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const token = req.headers.authorization.split(' ')[1];
      const user = jwtDecodeUser(token);

      const data = await OrdersEntity.findOne({
        id: parseInt(req.params['orderId']),
      });


      if (!data) {
        res.status(404).send({
          code: 404,
          message: "Not Found"
        })
      } else {
        if (data.uid != user.email) {
          res.status(403).send({
            code: 403,
            message: "Not Authorized"
          })
        } else {
          if (data.status === 'paid') {
            res.status(409).send({
              code: 409,
              message: "Already Paid"
            })
          } else {
            data.status = "paid";
            const newData = await data.save();

            res.send(newData)
          }
        }
      }
    } catch (e) {
      res.sendStatus(500);
    }
  };
}

export default IndexController;
